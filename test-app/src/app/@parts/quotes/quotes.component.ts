import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss']
})
export class QuotesComponent implements OnInit, OnDestroy {

  quotes = [
    {
      quote: 'You can avoid reality, but you cannot avoid the consequences of avoiding reality.',
      author: 'Ayn Rand'
    },
    {
      quote: 'I can write better than anybody who can write faster, and I can write faster than anybody who can write better.',
      author: 'A. J. Liebling'
    },
    {
      quote: 'I\'m all in favor of keeping dangerous weapons out of the hands of fools. Let\'s start with typewriters.',
      author: 'Frank Lloyd Wright'
    },
    {
      quote: 'A mathematician is a device for turning coffee into theorems.',
      author: 'Paul Erdos'
    }
  ];
  selectedQuote: {quote, author};
  quotesInterval;

  constructor() {
  }

  ngOnInit() {
    this.selectedQuote = this.getRandomQuote();
    this.quotesInterval = setInterval(() => {
      this.selectedQuote = this.getRandomQuote();
    }, 10000);
  }

  getRandomQuote() {
    return this.quotes[Math.floor(Math.random() * this.quotes.length)];
  }

  ngOnDestroy(): void {
    clearInterval(this.quotesInterval);
  }

}
