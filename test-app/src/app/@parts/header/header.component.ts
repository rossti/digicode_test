import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PreviewDialogComponent} from '../../@util/preview-dialog/preview-dialog.component';
import {MatDialog} from '@angular/material';
import {UploadDialogComponent} from '../../@util/upload-dialog/upload-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input()
  isLogged: boolean = false;

  constructor(private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
  }

  logout() {
    this.router.navigate(['/login']);
  }

  uploadFiles() {
    this.dialog.open(UploadDialogComponent, {
      data: {
        message: ''
      },
      panelClass: 'black-bg'
    });
  }

}
