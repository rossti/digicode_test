import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GiphyService {
  apiKey = '1I9piOiDfZUSt8urkvyHYVM0ykqYSPHc';
  parameter;

  constructor(private http: HttpClient) {}

  findGiphy(parameter, itemsCount) {
    this.parameter = parameter;
    return this.http.get<any>(`http://api.giphy.com/v1/gifs/search?q=${parameter}&api_key=${this.apiKey}&limit=${itemsCount}`);
  }

  findForAppend(offset, itemsCount) {
    return this.http.get<any>(
      `http://api.giphy.com/v1/gifs/search?q=${this.parameter}&api_key=${this.apiKey}&limit=${itemsCount}&offset=${offset}`
    );
  }

  findById(id) {
    return this.http.get<any>(`http://api.giphy.com/v1/gifs/${id}?api_key=${this.apiKey}`);
  }

  async upload(item: File, tags) {
    const formData: FormData = new FormData();
    formData.append('file', item);
    formData.append('tags', tags);
    formData.append('api_key', this.apiKey);

    const req = new HttpRequest('POST', `http://upload.giphy.com/v1/gifs?api_key=${this.apiKey}`, formData);
    // return this.http.request(req).subscribe((j: HttpResponse<any>) => {
    //   if (j.body) {
    //     return j.body.data.id;
    //   }
    // });
    return await this.http.request(req).toPromise() as HttpResponse<any>;
  }
}
