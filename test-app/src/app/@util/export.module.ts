import {NgModule} from '@angular/core';
import {HeaderComponent} from '../@parts/header/header.component';
import {CommonModule} from '@angular/common';
import {LogoComponent} from '../@parts/logo/logo.component';
import {RouterModule} from '@angular/router';
import { PreviewDialogComponent } from './preview-dialog/preview-dialog.component';
import {MatButtonModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import { UploadDialogComponent } from './upload-dialog/upload-dialog.component';
import {AuthDialogComponent} from './auth-dialog/auth-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFileUploadModule} from 'mat-file-upload';

@NgModule({
  declarations: [
    HeaderComponent,
    LogoComponent,
    PreviewDialogComponent,
    UploadDialogComponent,
    AuthDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFileUploadModule,
  ],
  exports: [
    HeaderComponent,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule
    // RouterModule
  ],
  entryComponents: [PreviewDialogComponent, UploadDialogComponent, AuthDialogComponent]
})
export class ExportModule {

}
