import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class CollectionService {
  myCollection = JSON.parse(localStorage.getItem('myCollection')) || [];
  reloadCollection: EventEmitter<any> = new EventEmitter();

  checkElInCollection(id): boolean {
    if (!localStorage.getItem('myCollection')) return false;

    const collection = JSON.parse(localStorage.getItem('myCollection'));
    return collection.some(item => {
      return item.id === id;
    });
  }

  changeCollection(item): void {
    if (!localStorage.getItem('myCollection')) { // if storage empty
      this.myCollection.push(item);
      localStorage.setItem('myCollection', JSON.stringify(this.myCollection));
    } else {
      if (this.checkElInCollection(item.id)) {
        this.myCollection = this.myCollection.filter(j => j.id !== item.id); // remove item from collection
        localStorage.setItem('myCollection', JSON.stringify(this.myCollection));
      } else {
        this.myCollection.push(item);
        localStorage.setItem('myCollection', JSON.stringify(this.myCollection)); // add item to collection
      }
    }
  }
}
