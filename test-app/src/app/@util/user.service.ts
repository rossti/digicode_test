import {Injectable} from '@angular/core';

@Injectable()
export class UserService {
  private username = 'admin';
  private password = 'admin';

  isValid() {
    return localStorage.getItem('username') === this.username && localStorage.getItem('password') === this.password;
  }
}
