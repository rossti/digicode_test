import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {GiphyService} from '../giphy.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CollectionService} from '../collection.service';
import {debounce, debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {
  uploadForm: FormGroup;
  uploadedFile;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog,
              private giphyService: GiphyService, private formBuilder: FormBuilder,
              private collectionService: CollectionService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.uploadForm = this.formBuilder.group({
      tag: [null]
    });
  }

  uploadFileChange(event) {
    console.log(event);
    if (event) {
      this.uploadedFile = event[0];
    }

  }

  uploadFile() {
    if (!this.uploadedFile || this.uploadedFile.type !== 'image/gif') {
      return false;
    }
    this.dialog.closeAll();
    const tag = this.uploadForm.controls.tag.value ? this.uploadForm.controls.tag.value : 'mygif';
    this.giphyService.upload(this.uploadedFile, tag).then(j => {
      this.giphyService.findById(j.body.data.id).subscribe(k => {
        console.log(k);
        this.collectionService.changeCollection(k.data);
        this.collectionService.reloadCollection.emit();
      });
    });
  }

}
