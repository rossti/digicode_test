import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthComponent } from './view/auth/auth.component';
import {AppRoutingModule} from './app.routing';
import { AuthFormComponent } from './view/auth/auth-form/auth-form.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material';
import {AuthGuardService} from './@util/auth-guard.service';
import {UserService} from './@util/user.service';
import { QuotesComponent } from './@parts/quotes/quotes.component';
import {ExportModule} from './@util/export.module';
import {GiphyService} from './@util/giphy.service';
import {HttpClientModule} from '@angular/common/http';
import {CollectionService} from './@util/collection.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    AuthFormComponent,
    QuotesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    ExportModule,
    HttpClientModule
  ],
  providers: [AuthGuardService, UserService, GiphyService, CollectionService],
  bootstrap: [AppComponent],
})
export class AppModule { }
