import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GiphyService} from '../../../@util/giphy.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchForm: FormGroup;
  searchResult;
  requestItemDefault = 50;
  offsetItemsCounter = this.requestItemDefault;

  constructor(private formBuilder: FormBuilder, private giphyService: GiphyService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.searchForm = this.formBuilder.group({
      search_parameter: [null, [Validators.required]],
    });
  }

  search() {
    if (this.searchForm.invalid) {
      return;
    }
    this.giphyService.findGiphy(this.searchForm.controls.search_parameter.value, this.requestItemDefault).subscribe(j => {
      this.searchResult = j;
    });
  }

  appendItems() {
    this.giphyService.findForAppend(this.offsetItemsCounter, this.requestItemDefault).subscribe(j => {
      for (let item = 0; item < j.data.length; item++) {
        this.searchResult.data.push(j.data[item]);
      }
      this.offsetItemsCounter += this.requestItemDefault;
    });
  }
}
