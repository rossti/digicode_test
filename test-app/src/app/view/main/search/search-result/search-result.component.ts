import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { NgxMasonryOptions } from 'ngx-masonry';
import {CollectionService} from '../../../../@util/collection.service';
import {MatDialog} from '@angular/material';
import {PreviewDialogComponent} from '../../../../@util/preview-dialog/preview-dialog.component';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  masnryOptions: NgxMasonryOptions = {
    gutter: 20
  };
  searchData;
  @Input()
  set _searchData(value) {
    if (value) {
      this.searchData = value;
      console.log(value);
    }
  };
  @Output()
  infiniteScroll: EventEmitter<any> = new EventEmitter();

  constructor(private collectionService: CollectionService, public dialog: MatDialog) { }

  ngOnInit() {

  }

  onScrollDown(): void {
    this.infiniteScroll.emit();
  }

  changeCollection(item): void {
    this.collectionService.changeCollection(item);
  }

  isInCollection(id): boolean {
    return this.collectionService.checkElInCollection(id);
  }

  showPreview(item) {
    this.dialog.open(PreviewDialogComponent, {
      data: {
        url: item.images.original.url,
        title: item.title
      }
    });
  }

}
