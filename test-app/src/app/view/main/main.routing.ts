import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main.component';
import {AuthGuardService} from '../../@util/auth-guard.service';
import {NgModule} from '@angular/core';
import {MyCollectionComponent} from './my-collection/my-collection.component';
import {SearchComponent} from './search/search.component';

const appRoutes: Routes = [
  {
    path: '', component: MainComponent, children: [
      {path: '', redirectTo: 'search', pathMatch: 'full'},
      {path: 'search', component: SearchComponent, canActivate: [AuthGuardService]},
      {path: 'my-collection', component: MyCollectionComponent, canActivate: [AuthGuardService]}
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
})
export class MainRouting {

}
