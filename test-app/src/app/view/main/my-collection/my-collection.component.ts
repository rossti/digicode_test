import { Component, OnInit } from '@angular/core';
import {CollectionService} from '../../../@util/collection.service';
import {MatDialog} from '@angular/material';
import {PreviewDialogComponent} from '../../../@util/preview-dialog/preview-dialog.component';

@Component({
  selector: 'app-my-collection',
  templateUrl: './my-collection.component.html',
  styleUrls: ['./my-collection.component.scss']
})
export class MyCollectionComponent implements OnInit {
  myCollection = [];

  constructor(private collectionService: CollectionService, public dialog: MatDialog) { }

  ngOnInit() {
    this.collectionInit();
    this.collectionService.reloadCollection.subscribe(j => {
      this.collectionInit();
    });
  }

  changeCollection(item): void {
    this.collectionService.changeCollection(item);
    this.collectionInit();
  }

  isInCollection(id): boolean {
    return this.collectionService.checkElInCollection(id);
  }

  collectionInit() {
    this.myCollection = JSON.parse(localStorage.getItem('myCollection'));
  }

  showPreview(item) {
    this.dialog.open(PreviewDialogComponent, {
      data: {
        url: item.images.original.url,
        title: item.title
      }
    });
  }

}
