import {NgModule} from '@angular/core';
import {MainComponent} from './main.component';
import {MainRouting} from './main.routing';
import {ExportModule} from '../../@util/export.module';
import { SearchComponent } from './search/search.component';
import { MyCollectionComponent } from './my-collection/my-collection.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import { SearchResultComponent } from './search/search-result/search-result.component';
import {NgxMasonryModule} from 'ngx-masonry';
import {CommonModule} from '@angular/common';
import {GiphyService} from '../../@util/giphy.service';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    MainComponent,
    SearchComponent,
    MyCollectionComponent,
    SearchResultComponent,
  ],
  imports: [
    MainRouting,
    ExportModule,
    NgxMasonryModule,
    CommonModule,
    InfiniteScrollModule
  ],
  exports: [
    MainComponent,
    SearchComponent,
    MyCollectionComponent,
  ],
})
export class MainModule {

}
