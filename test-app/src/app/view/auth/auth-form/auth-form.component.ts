import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../@util/user.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {AuthDialogComponent} from '../../../@util/auth-dialog/auth-dialog.component';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss']
})
export class AuthFormComponent implements OnInit {

  authForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router,
              public dialog: MatDialog) {}

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.authForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.authForm.controls[controlName];
    return control.invalid && control.touched;
  }

  signIn(): void {
    const controls = this.authForm.controls;
    if (this.authForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    localStorage.setItem('username', this.authForm.controls.username.value);
    localStorage.setItem('password', this.authForm.controls.password.value);

    if (this.userService.isValid()) {
      this.router.navigate(['/home']);
    } else {
      this.showAuthDialog('Username or password is incorrect. Please, use next data to sign in: username: admin, password: admin');
    }
  }

  showAuthDialog(message): void {
    this.dialog.open(AuthDialogComponent, {
      data: {
        message
      }
    });
  }

}
