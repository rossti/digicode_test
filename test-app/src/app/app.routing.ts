import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthComponent} from './view/auth/auth.component';
import {MainComponent} from './view/main/main.component';
import {AuthGuardService} from './@util/auth-guard.service';

const appRoutes: Routes = [
  {
    path: 'login', component: AuthComponent
  },
  {
    path: 'home',
    loadChildren: () => import('./view/main/main.module').then(m => m.MainModule),
    canActivate: [AuthGuardService]
  },
  {path: '**', redirectTo: 'login', pathMatch: 'full'},
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {

}
